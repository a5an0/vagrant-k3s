# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'etc'

# Use 2 CPU cores for the master and then 1 for each worker
NUM_WORKERS = Etc.nprocessors - 2

Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1804"

  config.vm.provider "libvirt" do |libvirt|
    libvirt.memory = "512"
    libvirt.cpus = 1
  end

  config.vm.define "controller" do |c|
    c.vm.hostname = "controller"
    c.vm.network "private_network", ip: "192.168.199.10"

    c.vm.provider "libvirt" do |libvirt|
      libvirt.memory = "640"
      libvirt.cpus = 2
    end

    c.vm.synced_folder "./config", "/vagrant", create: true, nfs_udp: false
    c.vm.provision "shell",
                   inline: %q(
curl -sfL https://get.k3s.io | sh -
export IP=$(ip addr show eth0 | grep -o "192\.168\.[0-9]\{1,3\}\.[0-9]\{1,3\}" | head -1)
echo master IP is $IP
echo $IP > /vagrant/controller_ip
sudo cat /var/lib/rancher/k3s/server/node-token > /vagrant/token
sudo sed  "s/127.0.0.1/$IP/g" /etc/rancher/k3s/k3s.yaml > /vagrant/k3s_config
)
  end

  (1..NUM_WORKERS).each do |n|
    config.vm.define "worker-#{n}" do |c|
      c.vm.hostname = "worker-#{n}"
      c.vm.network "private_network", ip: "192.168.199.2#{n}"
      c.vm.synced_folder "./config", "/vagrant", create: true, nfs_udp: false
      c.vm.provision "shell",
                     inline: %q(
while [ ! -f /vagrant/token ]
do
  echo "Waiting for token to be populated..."
  sleep 2
done
curl -sfL https://get.k3s.io | K3S_URL=https://192.168.199.10:6443 K3S_TOKEN=$(cat /vagrant/token) sh -
)
    end
  end
end
